package com.example.sineguzov.dao;

import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.entity.TeachersEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface StudentsMapper {

    @Select("select * from students where student_id = #{id} ")
    Optional<StudentsEntity> getStudentById(Integer id);

    @Select("select * from students")
    List<StudentsEntity> getAll();

    @Insert("insert into students(student_id, first_name, last_name, patronymic, course, specialty) VALUES" +
            " (#{studentId}, #{firstName}, #{lastName}, #{patronymic}, #{course}, #{specialty})")
    @SelectKey(keyProperty = "studentId", before = true, resultType = Integer.class,
            statement = "select nextval('students_seq')")
    void insert(StudentsEntity entity);

    @Delete("delete from students where student_id = #{id}")
    void deleteStudentById(Integer id);

    @Update("update students " +
            "set " +
            "first_name = #{entity.firstName}, " +
            "last_name = #{entity.lastName}, " +
            "patronymic = #{entity.patronymic}, " +
            "course = #{entity.course}, " +
            "specialty = #{entity.specialty} " +
            "where student_id = #{id}")
    void updateStudent(Integer id, StudentsEntity entity);

    @Select("select t.teacher_id, t.first_name, t.last_name, t.patronymic, t.chair\n" +
            "from teachers_to_students tts\n" +
            "    inner join teachers t\n" +
            "        on t.teacher_id = tts.teacher_id\n" +
            "where student_id = #{id}")
    List<TeachersEntity> getTeachersStudentById(Integer id);

    @Delete("delete from teachers_to_students where student_id = #{id}")
    void deleteStudentFromTeachersById(Integer id);

}

package com.example.sineguzov.dao;

import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.entity.TeachersEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface TeachersMapper {

    @Select("select * from teachers where teacher_id = #{id} ")
    Optional<TeachersEntity> getTeacherById(Integer id);

    @Select("select * from teachers")
    List<TeachersEntity> getAll();

    @Insert("insert into teachers(teacher_id, first_name, last_name, patronymic, chair) VALUES" +
            " (#{teacherId}, #{firstName}, #{lastName}, #{patronymic}, #{chair})")
    @SelectKey(keyProperty = "teacherId", before = true, resultType = Integer.class,
            statement = "select nextval('teachers_seq')")
    void insert(TeachersEntity entity);

    @Delete("delete from teachers where teacher_id = #{id}")
    void deleteTeacherById(Integer id);

    @Update("update teachers " +
            "set " +
            "first_name = #{entity.firstName}, " +
            "last_name = #{entity.lastName}, " +
            "patronymic = #{entity.patronymic}, " +
            "chair = #{entity.chair} " +
            "where teacher_id = #{id}")
    void updateTeacher(Integer id, TeachersEntity entity);

    @Select("select s.student_id, s.first_name, s.last_name, s.patronymic, s.course, s.specialty\n" +
            "from teachers_to_students tts\n" +
            "    inner join students s\n" +
            "        on s.student_id = tts.student_id\n" +
            "where teacher_id = #{id}")
    List<StudentsEntity> getStudentsByTeacherId(Integer id);

    @Delete("delete from teachers_to_students where teacher_id = #{id}")
    void deleteTeacherFromStudentsById(Integer id);

    @Delete("delete from teachers_to_students where teacher_id = #{teacherId} and student_id = #{studentId}")
    void deleteTeacherAndStudentById(Integer teacherId, Integer studentId);

    @Insert("insert into teachers_to_students(teacher_id, student_id) VALUES (#{teacherId}, #{studentId})")
    void insertTeacherAndStudent(Integer teacherId, Integer studentId);
}

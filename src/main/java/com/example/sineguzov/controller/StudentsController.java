package com.example.sineguzov.controller;

import com.example.sineguzov.dto.StudentsDTO;
import com.example.sineguzov.dto.TeachersDTO;
import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.service.api.StudentsService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentsController {

    private final StudentsService studentsService;

    @Autowired
    public StudentsController(StudentsService studentsService) {
        this.studentsService = studentsService;
    }

    @Operation(summary = "Получение всех студентов")
    @GetMapping
    public List<StudentsEntity> getAll() {
        return studentsService.getAll();
    }

    @Operation(summary = "Получение студента по id")
    @GetMapping("/{studentId}")
    public StudentsDTO getStudentById(@PathVariable Integer studentId) {
        return studentsService.getStudentById(studentId);
    }

    @Operation(summary = "Добавление нового студента")
    @PostMapping
    public StudentsDTO insertStudent(@RequestBody StudentsDTO dto) {
        return studentsService.insert(dto);
    }

    @Operation(summary = "Обновление информации о студенте")
    @PutMapping
    public StudentsDTO updateStudent(@RequestBody StudentsDTO dto) {
        studentsService.updateStudent(dto);
        return dto;
    }

    @Operation(summary = "Удаление студента по id")
    @DeleteMapping("/{studentId}")
    public String deleteStudent(@PathVariable Integer studentId) {
        return studentsService.deleteStudentById(studentId);
    }

    @Operation(summary = "Получение преподавателей у студента")
    @GetMapping("{studentId}/teachers")
    public List<TeachersDTO> getTeachers(@PathVariable Integer studentId) {
        return studentsService.getTeachersStudentById(studentId);
    }
}

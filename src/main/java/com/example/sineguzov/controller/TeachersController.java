package com.example.sineguzov.controller;

import com.example.sineguzov.dto.StudentsDTO;
import com.example.sineguzov.dto.TeachersDTO;
import com.example.sineguzov.entity.TeachersEntity;
import com.example.sineguzov.service.api.StudentsService;
import com.example.sineguzov.service.api.TeachersService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/teachers")
public class TeachersController {

    private final TeachersService teachersService;
    private final StudentsService studentsService;

    @Autowired
    public TeachersController(TeachersService teachersService, StudentsService studentsService) {
        this.teachersService = teachersService;
        this.studentsService = studentsService;
    }


    @Operation(summary = "Получение всех преподавателей")
    @GetMapping
    public List<TeachersEntity> getAll() {
        return teachersService.getAll();
    }

    @Operation(summary = "Получение преподавателя по id")
    @GetMapping("/{teacherId}")
    public TeachersDTO getTeacherById(@PathVariable Integer teacherId) {
        return teachersService.getTeacherById(teacherId);
    }

    @Operation(summary = "Добавление нового преподавателя")
    @PostMapping
    public TeachersDTO insertTeacher(@RequestBody TeachersDTO dto) {
        return teachersService.insert(dto);
    }

    @Operation(summary = "Обновление информации о преподавателе")
    @PutMapping
    public TeachersDTO updateTeacher(@RequestBody TeachersDTO dto) {
        teachersService.updateTeacher(dto);
        return dto;
    }

    @Operation(summary = "Удаление преподавателя по id")
    @DeleteMapping("/{teacherId}")
    public String deleteTeacher(@PathVariable Integer teacherId) {
        return teachersService.deleteTeacherById(teacherId);
    }

    @Operation(summary = "Получение студентов у преподавателя по id")
    @GetMapping("{teacherId}/students")
    public List<StudentsDTO> getStudents(@PathVariable Integer teacherId) {
        return teachersService.getStudentsByTeacherId(teacherId);
    }

    @Operation(summary = "Удаление студента у преподавателя по id")
    @DeleteMapping("{teacherId}/deleteStudent/{studentId}")
    public String deleteTeacherAndStudent(@PathVariable Integer teacherId, @PathVariable Integer studentId) {
        return teachersService.deleteTeacherAndStudentById(teacherId, studentId);
    }

    @Operation(summary = "Добавление студента к преподавателю по id")
    @PostMapping("{teacherId}/addStudent/{studentId}")
    public Map<TeachersDTO, StudentsDTO> addTeacherAndStudent(@PathVariable Integer teacherId, @PathVariable Integer studentId) {
        teachersService.insertTeacherAndStudent(teacherId, studentId);
        Map<TeachersDTO, StudentsDTO> map = new HashMap<>();
        map.put(teachersService.getTeacherById(teacherId), studentsService.getStudentById(studentId));
        return map;
    }
}

package com.example.sineguzov.grpc;

import com.example.grpc.OperationServiceGrpc;
import com.example.grpc.Serviceoperations;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
public class GrpcServer extends OperationServiceGrpc.OperationServiceImplBase {

    @Override
    public void getOperation(Serviceoperations.OperationRequest request, StreamObserver<Serviceoperations.OperationResponse> responseObserver) {

        log.info(request.getName());

        Serviceoperations.OperationResponse response = Serviceoperations.OperationResponse.newBuilder().build();

        responseObserver.onNext(response);

        responseObserver.onCompleted();
    }

    public void serverStart() throws IOException, InterruptedException {
        Server myServer = ServerBuilder
                .forPort(8082)
                .addService(new GrpcServer())
                .build();

        myServer.start();

        System.out.println("Server started");

        myServer.awaitTermination();
    }
}

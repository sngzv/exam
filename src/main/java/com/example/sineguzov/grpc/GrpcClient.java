package com.example.sineguzov.grpc;

import com.example.grpc.OperationServiceGrpc;
import com.example.grpc.Serviceoperations;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class GrpcClient {

    public void sendOperationToServer(String operation) {

        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8082")
                .usePlaintext().build();

        OperationServiceGrpc.OperationServiceBlockingStub stub =  OperationServiceGrpc.newBlockingStub(channel);

        Serviceoperations.OperationRequest request = Serviceoperations.OperationRequest
                .newBuilder().setName(operation).build();

        Serviceoperations.OperationResponse response = stub.getOperation(request);

        System.out.println(response);

        channel.shutdownNow();
    }
}

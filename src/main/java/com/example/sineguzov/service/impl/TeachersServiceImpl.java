package com.example.sineguzov.service.impl;

import com.example.sineguzov.dao.StudentsMapper;
import com.example.sineguzov.dao.TeachersMapper;
import com.example.sineguzov.dto.StudentsDTO;
import com.example.sineguzov.dto.TeachersDTO;
import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.entity.TeachersEntity;
import com.example.sineguzov.grpc.GrpcClient;
import com.example.sineguzov.service.api.TeachersService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class TeachersServiceImpl implements TeachersService {

    private final TeachersMapper teachersMapper;
    private final ModelMapper modelMapper;
    private final GrpcClient client;

    @Autowired
    public TeachersServiceImpl(TeachersMapper teachersMapper, ModelMapper modelMapper, GrpcClient client) {
        this.teachersMapper = teachersMapper;
        this.modelMapper = modelMapper;
        this.client = client;
    }

    @Override
    public TeachersDTO getTeacherById(Integer id) {
        TeachersEntity teachersEntity = teachersMapper.getTeacherById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность преподавателя по идентификатору=%s", id)
                )
        );
        return modelMapper.map(teachersEntity, TeachersDTO.class);
    }

    @Override
    public List<TeachersEntity> getAll() {
        List<TeachersEntity> entities = teachersMapper.getAll();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public TeachersDTO insert(TeachersDTO dto) {
        TeachersEntity teachersEntity = modelMapper.map(dto, TeachersEntity.class);
        teachersMapper.insert(teachersEntity);
        client.sendOperationToServer("Добавлен преподаватель : " + LocalDate.now());
        return getTeacherById(teachersEntity.getTeacherId());
    }

    @Override
    public String deleteTeacherById(Integer id) {
        deleteTeacherFromStudentsById(id);
        teachersMapper.deleteTeacherById(id);
        client.sendOperationToServer("Удален преподаватель по id " + id + " : " + LocalDate.now());
        return "delete teacher id= " + id;
    }

    @Override
    public TeachersDTO updateTeacher(TeachersDTO dto) {
        TeachersEntity entity = modelMapper.map(dto, TeachersEntity.class);
        teachersMapper.updateTeacher(entity.getTeacherId(), entity);
        client.sendOperationToServer("Обновлен преподаватель по id " + entity.getTeacherId() + " : " + LocalDate.now());
        return dto;
    }

    @Override
    public List<StudentsDTO> getStudentsByTeacherId(Integer id) {
        List<StudentsEntity> entities = teachersMapper.getStudentsByTeacherId(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public void deleteTeacherFromStudentsById(Integer id) {
        teachersMapper.deleteTeacherFromStudentsById(id);
    }

    @Override
    public String deleteTeacherAndStudentById(Integer teacherId, Integer studentId) {
        teachersMapper.deleteTeacherAndStudentById(teacherId, studentId);
        return String.format("delete teacher id = %d and student id = %d", teacherId, studentId);
    }

    @Override
    public void insertTeacherAndStudent(Integer teacherId, Integer studentId) {
        teachersMapper.insertTeacherAndStudent(teacherId, studentId);
    }
}

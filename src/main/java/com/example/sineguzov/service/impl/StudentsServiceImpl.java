package com.example.sineguzov.service.impl;

import com.example.sineguzov.dao.StudentsMapper;
import com.example.sineguzov.dto.StudentsDTO;
import com.example.sineguzov.dto.TeachersDTO;
import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.entity.TeachersEntity;
import com.example.sineguzov.grpc.GrpcClient;
import com.example.sineguzov.service.api.StudentsService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class StudentsServiceImpl implements StudentsService {

    private final StudentsMapper studentsMapper;
    private final ModelMapper modelMapper;
    private final GrpcClient client;

    @Autowired
    public StudentsServiceImpl(StudentsMapper studentsMapper, ModelMapper modelMapper, GrpcClient client) {
        this.studentsMapper = studentsMapper;
        this.modelMapper = modelMapper;
        this.client = client;
    }

    @Override
    public StudentsDTO getStudentById(Integer id) {
        StudentsEntity studentsEntity = studentsMapper.getStudentById(id).orElseThrow(
                () -> new RuntimeException(
                        String.format("Не найдена сущность студента по идентификатору=%s", id)
                )
        );
        return modelMapper.map(studentsEntity, StudentsDTO.class);

    }

    @Override
    public List<StudentsEntity> getAll() {
        List<StudentsEntity> entities = studentsMapper.getAll();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());

    }

    @Override
    public StudentsDTO insert(StudentsDTO dto) {
        StudentsEntity studentsEntity = modelMapper.map(dto, StudentsEntity.class);
        studentsMapper.insert(studentsEntity);
        client.sendOperationToServer("Добавлен студент : " + LocalDate.now());
        return getStudentById(studentsEntity.getStudentId());

    }

    @Override
    public String deleteStudentById(Integer id) {
        deleteStudentFromTeachersById(id);
        studentsMapper.deleteStudentById(id);
        client.sendOperationToServer("Удален студент по id " + id + " : " + LocalDate.now());
        return "delete student id= " + id;

    }

    @Override
    public StudentsDTO updateStudent(StudentsDTO dto) {
        StudentsEntity entity = modelMapper.map(dto, StudentsEntity.class);
        studentsMapper.updateStudent(entity.getStudentId(), entity);
        client.sendOperationToServer("Обновлен студент по id " + entity.getStudentId() + " : " + LocalDate.now());
        return dto;
    }

    @Override
    public List<TeachersDTO> getTeachersStudentById(Integer id) {
        List<TeachersEntity> entities = studentsMapper.getTeachersStudentById(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public void deleteStudentFromTeachersById(Integer id) {
        studentsMapper.deleteStudentFromTeachersById(id);
    }
}

package com.example.sineguzov.service.api;

import com.example.sineguzov.dto.StudentsDTO;
import com.example.sineguzov.dto.TeachersDTO;
import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.entity.TeachersEntity;

import java.util.List;
import java.util.Optional;

public interface StudentsService {

    StudentsDTO  getStudentById(Integer id);

    List<StudentsEntity> getAll();

    StudentsDTO insert(StudentsDTO dto);

    String deleteStudentById(Integer id);

    StudentsDTO updateStudent(StudentsDTO dto);

    List<TeachersDTO> getTeachersStudentById(Integer id);

    void deleteStudentFromTeachersById(Integer id);
}

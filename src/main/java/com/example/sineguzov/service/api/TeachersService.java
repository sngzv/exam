package com.example.sineguzov.service.api;

import com.example.sineguzov.dto.StudentsDTO;
import com.example.sineguzov.dto.TeachersDTO;
import com.example.sineguzov.entity.StudentsEntity;
import com.example.sineguzov.entity.TeachersEntity;

import java.util.List;
import java.util.Map;

public interface TeachersService {

    TeachersDTO getTeacherById(Integer id);

    List<TeachersEntity> getAll();

    TeachersDTO insert(TeachersDTO dto);

    String deleteTeacherById(Integer id);

    TeachersDTO updateTeacher(TeachersDTO dto);

    List<StudentsDTO> getStudentsByTeacherId(Integer id);

    void deleteTeacherFromStudentsById(Integer id);

    String deleteTeacherAndStudentById(Integer teacherId, Integer studentId);

    void insertTeacherAndStudent(Integer teacherId, Integer studentId);
}

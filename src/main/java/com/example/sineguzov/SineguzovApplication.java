package com.example.sineguzov;

import com.example.sineguzov.grpc.GrpcServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class SineguzovApplication {

    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplication.run(SineguzovApplication.class, args);
        new GrpcServer().serverStart();
    }

}

create sequence students_seq;

create table Students (
                          student_id int not null default nextval('students_seq'),
                          first_name varchar(25),
                          last_name varchar(30),
                          patronymic varchar(30),
                          course int,
                          specialty varchar(30)
);

alter table students
    add constraint students_pk
        primary key (student_id);

create sequence teachers_seq;

create table Teachers (
                          teacher_id int not null default nextval('teachers_seq'),
                          first_name varchar(25),
                          last_name varchar(30),
                          patronymic varchar(30),
                          chair varchar(40)
);

alter table teachers
    add constraint teachers_pk
        primary key (teacher_id);

create table teachers_to_students (
                                      teacher_id int not null,
                                      student_id int not null,
                                      primary key (teacher_id, student_id),
                                      foreign key (teacher_id) references teachers (teacher_id),
                                      foreign key (student_id) references students (student_id)
);